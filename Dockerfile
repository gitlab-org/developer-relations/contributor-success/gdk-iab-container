FROM debian:latest

WORKDIR /gitlab-gdk

# Prepare the 'raw' container image for GDK
RUN apt-get update \
    && apt-get install -y \
      cmake \
      curl \
      g++ \
      git \
      libssl-dev \
      locales \
      make \
      pkg-config \
      software-properties-common \
      sudo \
      vim \
      chromium-driver \
    && rm -rf /var/lib/apt/lists/* \
    && useradd gdk -u 5001 -m -s /bin/bash \
    && chown -Rv gdk:gdk /gitlab-gdk \
    && sed -i "s|# en_US.UTF-8 UTF-8|en_US.UTF-8 UTF-8|" /etc/locale.gen \
    && sed -i "s|# C.UTF-8 UTF-8|C.UTF-8 UTF-8|" /etc/locale.gen \
    && locale-gen C.UTF-8 en_US.UTF-8 \
    && mkdir -p /run/sshd \
    && mv /etc/ssh /etc/ssh-bootstrap \
    && mkdir /etc/ssh

COPY --chmod=600 --chown=root:root ./gdk_sudoers /etc/sudoers.d/gdk_sudoers
COPY --chmod=755 --chown=gdk:gdk ./gdk-container-startup.sh /home/gdk/gdk-container-startup.sh

USER gdk

SHELL ["/bin/bash", "-c"]

# Prepare Mise
RUN curl https://sh.rustup.rs -sSf | sh -s -- -y \
    && . "$HOME/.cargo/env" \
    && cargo install mise \
    && eval "$(mise activate bash)" \
    && echo "# Added to support mise for GDK" >> ~/.bashrc \
    && echo ". \"$HOME/.cargo/env\"" >> ~/.bashrc \
    && echo "eval \"\$(mise activate bash)\"" >> ~/.bashrc

COPY --chmod=755 --chown=gdk:gdk ./lefthook-local.yml /gitlab-gdk/lefthook-local.yml

# Prepare GDK and prereqs (bootstrap)
RUN . "$HOME/.cargo/env" \
    && eval "$(mise activate bash)" \
    && git clone https://gitlab.com/gitlab-org/gitlab-development-kit.git \
    && cd gitlab-development-kit \
    && echo -e "---\nasdf:\n  opt_out: true\nmise:\n  enabled: true\n" > gdk.yml \
    && make bootstrap

# Prepare the container for SSH access
RUN mkdir -p ~/.ssh \
    && cp /gitlab-gdk/gitlab-development-kit/support/gdk-in-a-box/gdk.local_rsa.pub ~/.ssh/authorized_keys \
    && chmod 600 ~/.ssh/authorized_keys

WORKDIR /gitlab-gdk/gitlab-development-kit

COPY --chmod=755 --chown=gdk:gdk ./install-config-gdk.sh /gitlab-gdk/gitlab-development-kit/install-config-gdk.sh
  
# Install and configure GDK
RUN ./install-config-gdk.sh

VOLUME /etc/ssh

EXPOSE 22/tcp
EXPOSE 2222/tcp
EXPOSE 3000/tcp
EXPOSE 3005/tcp
EXPOSE 3010/tcp
EXPOSE 3038/tcp
EXPOSE 5000/tcp
EXPOSE 5778/tcp
EXPOSE 9000/tcp
  
CMD ["/home/gdk/gdk-container-startup.sh"]
  