#!/usr/bin/env bash

set -euo pipefail

. "$HOME/.cargo/env"
eval "$(mise activate bash)"

mise x -- gdk install gitlab_repo="https://gitlab.com/gitlab-community/gitlab.git" telemetry_user="gdk-in-a-box"
mise x -- gdk config set hostname gdk.local
mise x -- gdk config set listen_address 0.0.0.0
mise x -- gdk config set webpack.enabled false
mise x -- gdk config set vite.enabled true
mise x -- gdk reconfigure
mise x -- gdk start
sleep 60s
cd gitlab && mise x -- bundle exec rspec spec/models/customer_relations/contact_spec.rb
mise x -- gdk stop
