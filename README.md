## Containerised GDK-in-a-box

For more GDK-in-a-box info, please check out: https://docs.gitlab.com/ee/development/contributing/first_contribution/configure-dev-env-gdk-in-a-box.html

### Usage

To run this container, the following command-line:

```shell
docker run -d -h gdk.local --network host --name gdk \
  -p 2022:22 \
  -p 2222:2222 \
  -p 3000:3000 \
  -p 3005:3005 \
  -p 3010:3010 \
  -p 3038:3038 \
  -p 5000:5000 \
  -p 5778:5778 \
  -p 9000:9000 \
  registry.gitlab.com/gitlab-org/developer-relations/contributor-success/gdk-iab-container:latest
```

### SSH Host Keys

You can add an optional volume for SSH host keys, which will be generated on first run if they don't exist. (If you don't use this volume, every time you create a new instance of the container, it'll regenerate SSH host keys, and you'll get a mismatch with your `~/.ssh/known_hosts`)

```shell
docker volume create gdk-ssh #only required once
docker run -d -h gdk.local --network host --name gdk \
  -v gdk-ssh:/etc/ssh \
  -p 2022:22 \
  -p 2222:2222 \
  -p 3000:3000 \
  -p 3005:3005 \
  -p 3010:3010 \
  -p 3038:3038 \
  -p 5000:5000 \
  -p 5778:5778 \
  -p 9000:9000 \
  registry.gitlab.com/gitlab-org/developer-relations/contributor-success/gdk-iab-container:latest
```

### Connecting to container

Once the container is up, you can treat this container as a regular "GDK-in-a-box" vm. To connect, you can SSH to the container (using the GDK-in-a-box keys):

```shell
ssh gdk@localhost -p 2022 -i ~/.ssh/gdk.local_rsa
```

NOTE: This assumes the default location for the SSH key.
