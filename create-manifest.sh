#!/bin/bash

usage() {
  echo "Error: ${1}"

  echo "Usage: ${0} <manifest name>"
}

if [ -z "${1}" ]; then
  usage "Must specify manifest name as first arg"
fi

MANIFEST_NAME=$1

docker manifest create "$MANIFEST_NAME" \
  --amend "$DOCKER_IMAGE_NAME-arm64" \
  --amend "$DOCKER_IMAGE_NAME-amd64"

docker manifest push "$MANIFEST_NAME"
