#!/usr/bin/bash

gen_ssh_host_key() {
  cipher=$1
  
  echo "Creating key for ${cipher}"
  sudo ssh-keygen -q -f "/etc/ssh/ssh_host_${cipher}_key" -N '' -t ${cipher}
  ssh-keygen -l -f "/etc/ssh/ssh_host_${cipher}_key.pub"
}

if [ ! -f /etc/ssh/sshd_config ]; then
  sudo cp -r /etc/ssh-bootstrap/* /etc/ssh/
  for key in rsa dsa ecdsa ed25519; do
    gen_ssh_host_key $key
  done
fi

sudo /usr/sbin/sshd

cd /gitlab-gdk/gitlab-development-kit

source "$HOME/.cargo/env" && eval "$(mise activate bash)"

mise x -- gdk restart

sleep 720d
